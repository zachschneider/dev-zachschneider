	$("#telephoneNumber[title]").tooltip({
		position: "center right",
		// tweak the position
        offset: [0, 10], //[up/down, left/right]
        // use the "slide" effect
        effect: 'fade',
    });

    $("#websiteURL[title]").tooltip({
		position: "center right",
        offset: [0, 10],
        effect: 'fade'
    });

    $("#productDescription[title]").tooltip({
		position: "center right",
        offset: [0, 10],
        effect: 'fade'
    });
	
	$("#adjLogo[title]").tooltip({
		position: "center right",
        offset: [0, 10],
        effect: 'fade'
    });

    $("#logoWorded[title]").tooltip({
		position: "center right",
        offset: [0, 10],
        effect: 'fade'
    });

    $("#appLogo[title]").tooltip({
		position: "center right",
        offset: [0, 10],
        effect: 'fade'
    });

    $("#budget[title]").tooltip({
		position: "center right",
        offset: [0, 10],
        effect: 'fade'
    });
