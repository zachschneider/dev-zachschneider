<?php 

$email=$_REQUEST['email']; 
$firstName=$_REQUEST['firstName']; 
$lastName=$_REQUEST['lastName']; 

//The Attachment 
$cr = "\n"; 
$data = "Email" . ',' . "First Name" . ',' . "Last Name" . $cr; 
$data .= "$email" . ',' . "$firstName" . ',' . "$lastName" . $cr; 
$fp = fopen('file.csv','a'); 
fwrite($fp,$data); 
fclose($fp); 

// Mail to 
$email = "zachschneider@gmail.com"; 

//subject 
$subject = "Test"; 

//Header 
$headers("Content-type: application/octet-stream"); 
$headers("Content-Disposition: attachment; filename=test.csv"); 
$headers("Pragma: no-cache"); 
$headers("Expires: 0"); 


//Message 
$message = "". 
"Email: $email" . "\n" . 
"First Name: $firstName" . "\n" . 
"Last Name: $lastName"; 

mail($email, $subject, $message, $headers); 
$emailSent = true;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Contact Form</title>
<link type="text/css" rel="stylesheet" href="formStyle.css" />
<script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="js/jquery.tools.min.js" type="text/javascript"></script>
<script src="js/jquery.validate.pack.js" type="text/javascript"></script>
<script src="js/formScript.js" type="text/javascript"></script>
</head>

<body>
<?php if(isset($emailSent) && $emailSent == true) { //If email is sent ?>
<style>
#formWrapper{display: none;}
</style>
<p><h1>Thank you!</h1></p>
<?php } ?>

<!-- START FORM WRAPPER -->
<div id="formWrapper">
	 <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="contactForm">
		<!-- START Tell us about Yourself Section -->
		<div id="tuay">
			<div class="leftCol">
				<h2>tell us about yourself</h2>
				<p>By filling out this form you are providing us with essential information for the development of your project.</p>
				<p>Thanks in advance for taking the time to fill this out.</p>
			</div>
			<div class="rightCol">
				<h4>Your Name<span>*</span></h4>
				<div class="tuayTitle box142">
					<input name="firstName" type="text" id="firstName" class="required" />
					<h5>first</h5>
				</div>
				<div class="tuayTitle box142">
					<input name="lastName" type="text" id="lastName" class="required" />
					<h5>last</h5>
				</div>
				
				<h4>Company Name<span>*</span></h4>
				<div class="tuayTitle">
					<input name="companyName" type="text" id="companyName" class="required" />
				</div>
				
				<h4>Email Address<span>*</span></h4>
				<input name="emailAddress" type="text" id="emailAddress" class="required email" />
				
				<h4>Telephone Number</h4>
				<input name="telephoneNumber" type="text" id="telephoneNumber" title="Live outside the U.S.? That’s cool. Could you please include your country code?" />
				
				<h4>Website URL</h4>
				<input name="websiteURL" type="text" id="websiteURL" value="http://" title="You know, if you have one" />
			</div>
		</div><!-- END Tell us about Yourself Section -->
		
		<!-- START WHAT CAN WE DO FOR YOU -->
		<div id="wcwdfy">
			<div class="leftCol">
				<h2>what can we do for you?</h2>
				<p>We offer a number of creative services to individuals and businesses. To get as clear a picture of your project prior to meeting, please answer in as much detail as possible.</p>
			</div>
			<div class="rightCol">
				<div class="box142">
					<input name="wcwdfy1" type="checkbox" value="identityDesign" id="identityDesign" />
					<label for="identityDesign">Identity Design</label> 
					<input name="wcwdfy1" type="checkbox" value="websiteDesign" id="websiteDesign" /> 
						<label for="websiteDesign">Website Design</label><br />
					<input name="wcwdfy1" type="checkbox" value="stationary" id="stationary" />
						<label for="stationary">Stationary / Business Collateral</label><br />
					<input name="wcwdfy1" type="checkbox" value="packaging" id="packaging" /> 
						<label for="packaging">Packaging</label> 
					<input name="wcwdfy1" type="checkbox" value="other" id="otherDo" /> 
						<label for="otherDo"><input name="wcwdfy1Other" type="text" id="wcwdfy1Other" value="other" /></label>
				</div>
				<p>Please describe your products or services<br />
				<textarea name="productDescription" id="productDescription" cols="3" rows="2" title="This is one of those times you need to be as specific as possible."></textarea></p>
				
				<p>Who are your main competitors? How do you differ from them? What would you like to achieve?<br />
				<textarea name="mainCompetitors" id="mainCompetitors" cols="3" rows="2"></textarea></p>
				
				<p>What is the age range of your target client base?</p>
				<div class="box50">
					<input name="wcwdfy2" type="radio" value="radio18_25" id="radio18_25" /> 
						<label for="radio18_25">18-25</label>
					<input name="wcwdfy2" type="radio" value="radio26_35" id="radio26_35" /> 
						<label for="radio26_35">26-35</label>
					<input name="wcwdfy2" type="radio" value="radio36_45" id="radio36_45" /> 
						<label for="radio36_45">36-45</label>
					<input name="wcwdfy2" type="radio" value="radio46"    id="radio46"    /> 
						<label for="radio46">46+</label>
					<input name="wcwdfy2" type="radio" value="radioOther2" id="radioOther2" /> 
						<label for="radioOther2">
							<input type="text" name="wcwdfy2Other" id="wcwdfy2Other" value="other" /></label>
				</div>
			</div>
		</div>
		<!-- END WHAT CAN WE DO FOR YOU -->

		<!-- START TELL US ABOUT YOUR COMPANY -->
		<div id="tuayc">
			<div class="leftCol">
				<h2>tell us about your company</h2>
				<p>This section targets specific questions in regards to you rcompany.</p>
			</div>
			<div class="rightCol">
			<p>What does your company name mean?<br />
			How was it thought up or what was it derived from?<br />
			<textarea name="companyMeaning" id="companyMeaning" cols="3" rows="2"></textarea></p>
			
			<p>What is your brand tagline? Do you want it stated within the logo?<br />
			<input name="brandLogo" type="text" id="brandLogo" /></p>
			
			<p>Do you have any specific imagery or icons you would like to appear in your logo?<br />
			<textarea name="imageryIcons" id="imageryIcons" cols="3" rows="2"></textarea></p>
			
			<p>Do you have any color preferences, existing brand colors, and/or colors you do not wish to include in your logo?<br />
			<textarea name="colorPref" id="colorPref" cols="3" rows="2"></textarea></p>
			
			<p>What adjectives would best describe your logo?<br />
			<textarea name="adjLogo" id="adjLogo" cols="3" rows="2" title="You remember adjectives right? They’re descriptors if you dont. Words like clean, strong or modern."></textarea></p>
			
			<p>What overall message do you want your logo to convey to your audience?<br />
			<textarea name="overallMessage" id="overallMessage" cols="3" rows="2"></textarea></p>
			
			<p>How do you prefer your logo worded or written?<br />
			<input type="text" name="logoWorded" id="logoWorded" title="Get tar, or guitar?" /></p>
			
			<p>Preferred typography<br /> (heavy, bold, hand written, script, light)<br />
			<input type="text" name="preferTypog" id="preferTypog" /></p>
			
			<p>Where will your logo be used?<br />
			<input name="logoUsed" type="radio" value="logoPrint" id="logoPrint" /> <label for="logoPrint">Print</label>
			<input name="logoUsed" type="radio" value="logoWeb" id="logoWeb" /> <label for="logoWeb">Web</label>
			<input name="logoUsed" type="radio" value="logoOther" id="logoOther" /> <label for="logoOther">Other</label></p>
			
			<p>What&rsquo;s the most important application for your logo?<br />
			<input type="text" name="appLogo" id="appLogo" title="Business cards, websites, promotional gifts, etc." /></p>
			
			<p>Do any existing company logos appeal to you?<br />
			Please provide website links if possible.<br />
			<textarea name="existingLogos" id="existingLogos" cols="3" rows="2"></textarea></p>
			</div>
		</div>
		<!-- END TELL US ABOUT YOUR COMPANY -->
		
		<!-- START IN REGARDS TO WEBSITE DESIGN -->
		<div id="irtwd">
			<div class="leftCol">
				<h2>in regards to website design</h2>
				<p>This section targets specific questions in regards to your www space.</p>
			</div>
			<div class="rightCol">
				<p>What are some existing websites that appeal to you? Provide links if possible.<br />
				<textarea name="existingWebsites" id="existingWebsites" cols="3" rows="2"></textarea></p>
				
				<p>What are your top 3 frustrations with your current website?<br />
				<textarea name="frustratingWebsite" id="frustratingWebsite" cols="3" rows="2"></textarea></p>
				
				<p>What do you most like about your current website?<br />
				<textarea name="likeWebsite" id="likeWebsite" cols="3" rows="2"></textarea></p>
				
				<p>Imagine your website 1 year from now. Complete the sentence: I know the website worked because...<br />
				<textarea name="imagineWebsite" id="imagineWebsite" cols="3" rows="2"></textarea></p>
				
				<p>Do you need to be able to update the website on your own? (ie: use a CMS)</p>
				<p><input type="radio" name="yesUpdate" id="yesUpdate" /> <label for="yesUpdate">Yes, I need it built on a Content Management System (CMS)</label></p>
				<p><input type="radio" name="noUpdate" id="noUpdate" /> <label for="noUpdate">No, I will not be doing any updating</label></p>
				<p><input type="radio" name="noCMS" id="noCMS" /> <label for="noCMS">I will be doing some updating, but no need for a CMS</label></p>
				
				<p>Will this be an e-commerce website? (ie: Will you be selling products on your website?)</p>
				<p><input type="radio" name="yesCommerce" id="yesCommerce" /> 
					<label for="yesCommerce">Yes, the purpose of my site is strictly e-commerce related.</label></p>
				<p><input type="radio" name="noCommerce" id="noCommerce" /> 
					<label for="noCommerce">No.</label></p>
				<p><input type="radio" name="lilCommerce" id="lilCommerce" /> 
					<label for="lilCommerce">A little product selling, but not a lot. Maybe, just a link to pay by PayPal or something similar.</label></p>
				
				<p>Do you have copy already written or do you need a copywriter?</p>
				<p><input type="radio" name="yesCopy" id="yesCopy" /> 
					<label for="yesCopy">Yes, I'm in need of a copywriter.</label></p>
				<p><input type="radio" name="noCopy" id="noCopy" /> 
					<label for="noCopy">You can use the existing copy of my older website..</label></p>
				<p><input type="radio" name="preCopy" id="preCopy" /> 
					<label for="preCopy">I will be writing the copy myself prior to the start of the design.</label></p>
			</div>
		</div>
		<div id="lastDetails">
			<div class="leftCol"></div>
			<div class="rightCol">
				<p>Deadline, timing or exact date of completion<br />
				<input name="deadline" type="text" id="deadline" /></p>
				
				<p>Please provide any additional details here<br />
				<textarea name="additionalDetails" id="additionalDetails" cols="3" rows="2"></textarea></p>
				
				<p>Budget<br />
				<input type="text" name="budget" id="budget" value="$" title="Budget = Time = Solution" /></p>
				
				<p>If you could be so kind to tell us how you found us?<br />
				<input name="foundUs" type="text" id="foundUs" /></p>
			</div>
		</div>
		<!-- END IN REGARDS TO WEBSITE DESIGN -->
		
		<!-- START SUBMIT -->
		<div id="formSubmit">
			<input name="sendBut" type="submit" id="sendBut" value="send" />
		</div>
	</form>
</div>
<!-- END FORM WRAPPER -->
</body>
</html>
