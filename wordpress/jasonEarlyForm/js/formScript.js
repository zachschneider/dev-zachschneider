$(document).ready(function () {
    $("#tuayc").css("display", "none");
    $("#irtwd").css("display", "none");
    $("#identityDesign").click(function () {

        // If checked
        if ($("#identityDesign").is(":checked")) {
            //show the hidden div
            $("#tuayc").show();
        } else {
            //otherwise, hide it 
            $("#tuayc").hide();
        }
    });

    $("#websiteDesign").click(function () {

        // If checked
        if ($("#websiteDesign").is(":checked")) {
            //show the hidden div
            $("#irtwd").show();
        } else {
            //otherwise, hide it 
            $("#irtwd").hide();
        }
    });

    
	$("#telephoneNumber[title]").tooltip({
		// position - center, top, left, right
		position: "center right",
		// tweak the position
        offset: [0, 10], //[up/down, left/right]
		events: {
			def:     "mouseenter,mouseleave",    // default show/hide events for an element
			input:   "focus,blur",               // for all input elements
			widget:  "focus mouseenter,blur mouseleave",  // select, checkbox, radio, button
			tooltip: "hover,mouseleave"     // the tooltip element
		}
	});

    $("#websiteURL[title]").tooltip({
		position: "center right",
        offset: [0, 10],
    });

    $("#productDescription[title]").tooltip({
		position: "center right",
        offset: [0, 10],
    });
	
	$("#adjLogo[title]").tooltip({
		position: "center right",
        offset: [0, 10],
    });

    $("#logoWorded[title]").tooltip({
		position: "center right",
        offset: [0, 10],
    });

    $("#appLogo[title]").tooltip({
		position: "center right",
        offset: [0, 10],
    });

    $("#budget[title]").tooltip({
		position: "center right",
        offset: [0, 10],
    });

// validates form
    $("#contactForm").validate();
});
