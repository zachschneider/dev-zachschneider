/* ===== custom tooltip ===== */
$(function(){
	$('input, textarea').each(function(e){
		var tooltip = $(this).next('.tooltip');
		
		$(this).hover(
		function(){
			var textCont = "<div class='tooltip'>"+$(this).attr('title')+"</div>";
			$(this).after(textCont);
			tooltip.show();
		},
		function(){
			$(this).next('.tooltip').remove();
			tooltip.hide();
		});
		
	});
});
