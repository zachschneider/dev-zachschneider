<?php
	date_default_timezone_set('America/Chicago');
	$today = getdate();
		if(isset($_GET['mon'])){
		   if(isset($_GET['year'])){
			  $start = mktime(0,0,0,$_GET['mon'],1,$_GET['year']);
		   }
		   else{
			  $start = mktime(0,0,0,$_GET['mon'],1,$today['year']);
		   }
		}
		else{
		   $start = mktime(0,0,0,$today['mon'],1,$today['year']);
		}
	$first = getdate($start);
	$end = mktime(0,0,0,$first['mon']+1,0,$first['year']);
	$last = getdate($end);
?>

<!-- begin sidebar -->
  	<div id="leftSide">
    <a href="<?php bloginfo('url'); ?>"><span id="logo"></span></a>	

<ul>
	<li id="categories">
		<ul>
			<?php wp_list_cats(); ?>
		</ul>
 	</li>
	<li>
	<div class="calendar">
  <div class="monheader">
	<table border="0" cellpadding="0" cellspacing="0" width="175">
		<tr>
			<td>jan</td>
			<td>feb</td>
			<td>mar</td>
			<td>apr</td>
		</tr>
		<tr>
			<td>may</td>
			<td>jun</td>
			<td>jul</td>
			<td>aug</td>
		</tr>
		<tr>
			<td>sep</td>
			<td>oct</td>
			<td>nov</td>
			<td>dec</td>
		</tr>
	</table>
  </div>
<?php
	for($i = 0; $i < $first['wday']; $i++){
	   echo '  <div class="inactive"></div>' . "\n";
	}
	for($i = 1; $i <= $last['mday']; $i++){
	   if($i == $today['mday']){
		  $style = 'today';
	   }
	   else{
		  $style = 'day';
	   }
	   echo '  <div class="' . $style . '">' . $i . '</div>' . "\n";
	}
	if($last['wday'] < 6){
	   for($i = $last['wday']; $i < 6; $i++){
		  echo '  <div class="inactive"></div>' . "\n";
	   }
	}
	//print_r ($today);
?>
</div>



	</li>
	<li id="tags">
		<ul>
			<?php wp_tag_cloud('separator= // <br>&smallest=12&largest=12&unit=px&number=11&order=ASC'); 
			echo(' // <br>'); ?>
		</ul>
	</li>
</ul>



<?php if ( !function_exists('dynamic_sidebar')
        || !dynamic_sidebar() ) : ?>

<ul>
	<?php wp_list_pages('title_li=<h2>Main Menu</h2>'); ?>
</ul>
</div>
<?php endif; ?>
</div>
<!-- end sidebar -->
