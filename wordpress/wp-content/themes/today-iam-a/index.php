<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div class="post">
<h3><?php the_category(',') ?>:</h3>
	 <h2 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
	<div class="meta">
	<span class="date"><?php the_time('m.d.Y'); ?></span> <?php the_tags('// ',' // '); ?> 
	<br/>
	<div class="feedback"><?php edit_post_link(__('edit this')); ?><?php echo (" |"); ?> <?php comments_popup_link(__('0 comments'), __('1 comments'), __('% comments')); ?></div>
	</div>
	
	<div class="storycontent">
		<?php the_content(__('(more...)')); ?>
	</div>
</div>



<?php comments_template(); // Get wp-comments.php template ?>

<?php endwhile; else: ?>
<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>

<?php posts_nav_link(' &#8212; ', __('&laquo; Previous Page'), __('Next Page &raquo;')); ?>


<?php get_footer(); ?>
