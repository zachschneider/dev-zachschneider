<?php

$subject = "MPD Foundation Whitepapers";



//If the form is submitted

if(isset($_POST['submit'])) {

	//Check to make sure that the name field is not empty

	if(trim($_POST['name']) == '') {

		$hasError = true;

	} else {

		$name = trim($_POST['name']);

	}



	//Check to make sure that a valid email address is submitted

	if(trim($_POST['email']) == '')  {

		$hasError = true;

	} else if (!eregi("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", trim($_POST['email']))) {

		$hasError = true;

	} else {

		$email = trim($_POST['email']);

	}

	

	//If there is no error, send the email

	if(!isset($hasError)) {

		$emailTo = 'zachschneider@gmail.com'; //Put your own email address here

		$body = "Name: $name \n\nEmail: $email";

		$headers = 'From: <'.$emailTo.'>' . "\r\n" . 'Reply-To: ' . $email;



		mail($emailTo, $subject, $body, $headers);

		$emailSent = true;

	}

}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="form.css" />
<script src="jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="jquery.validate.pack.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function(){

	$("#demo_form").validate();

});

</script>
</head>
<body>
<?php if(isset($hasError)) { //If errors are found ?>
<p class="error">Please check if you've filled all the fields with valid information.</p>
<?php } ?>
<?php if(isset($emailSent) && $emailSent == true) { //If email is sent ?>
<style>
#dowload_whitepaper{display: none;}
</style>
<p><b>Thank you!</b></p>
<?php } ?>
<div id="form_data" style="overflow: hidden;">
  <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="demo_form">
    <b>Name:</b><br/>
    <input id="name" name="name" class="required" type="text" size="15">
    <br/>
    <b>E-mail:</b><br/>
    <input id="email" name="email" class="required" type="text" size="15">
    <br/>
    <button type="submit" name="submit" class="red_btn">DOWNLOAD</button>
  </form>
</div>
</body>
</html>