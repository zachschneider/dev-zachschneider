(function($){
    jQuery.fn.extend({
        rssFeed: function(options) {  
  
            var defaults = { 
                url: '',
                html: '<h2><a href="{link}">{title}</a></h2>{text}<br /><a href="{more}"><img src="http://www.secondcitysoiree.com/storage/graphics/ClickForMore.png" alt="click for more" width="108" height="32" border="0" /></a>',
                wrapper: 'span',
                dataType: 'xml',
                display: 0,
            }
            var options = jQuery.extend(defaults, options);
  
            return this.each(function() { 
                var o = options;
                var c = jQuery(this);

                if (o.url == '') {
                    return; // avoid the request
                }

                jQuery.ajax({
                    url: o.url,
                    type: 'GET',
                    dataType: o.dataType,
                    error: function (xhr, status, e) {
                        console.debug('C: #%s, Error: %s, Feed: %s', $(c).attr('id'), e, o.url);
                    },
                    success: function(feed){

                        jQuery(feed).find('item').each(function(i){

                            var itemHtml = o.html.replace(/{title}/, jQuery(this).find('title').text());
                            itemHtml = itemHtml.replace(/{text}/, jQuery(this).find('description').text());
                            itemHtml = itemHtml.replace(/{link}/, jQuery(this).find('guid').text());
							itemHtml = itemHtml.replace(/{more}/, jQuery(this).find('guid').text());

                            jQuery(c).append(jQuery('<' + o.wrapper + '>').append(itemHtml));

                            if (i == o.display) {
                                return false;
                            }

                        });
                    }
                });
            });
            return this;
        }
    });
})(jQuery);